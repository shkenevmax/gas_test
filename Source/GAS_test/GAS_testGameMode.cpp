// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAS_testGameMode.h"
#include "GAS_testCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAS_testGameMode::AGAS_testGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
