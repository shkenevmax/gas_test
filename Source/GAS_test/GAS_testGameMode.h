// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAS_testGameMode.generated.h"

UCLASS(minimalapi)
class AGAS_testGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAS_testGameMode();
};



