// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "AbilitySystemInterface.h"
#include "GameplayEffectTypes.h"
#include "GAS_testCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
class UGAS_Component;
class UGAS_AttributeSet;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UCLASS(config=Game)
class AGAS_testCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

public:
	AGAS_testCharacter();
	
	virtual void PossessedBy(AController* NewController) override;
	virtual void OnRep_PlayerState() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Gameplay ability")
		class UGAS_Component* GAS_Component;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Gameplay ability")
		const class UGAS_AttributeSet* GAS_AttributeSet;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay ability")
		TArray<TSubclassOf<class UGameplayAbility>> InitialAbilities;

		virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION()
		virtual void OnHealthChangedNative(float Health, int32 StackCount);
	UFUNCTION()
		virtual void OnManaChangedNative(float Mana, int32 StackCount);
	UFUNCTION()
		virtual void OnBaseAttackDamageChangedNative(float BaseAttackDamage, int32 StackCount);
	UFUNCTION()
		virtual void OnSpeedMultiplierChangedNative(float SpeedMultiplier, int32 StackCount);
	UFUNCTION()
		virtual void OnFireBallCooldownChangedNative(float FireBallCooldown, int32 StackCount);

	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay ability")
		void OnHealthChange(float Health, int32 StackCount);
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay ability")
		void OnManaChange(float Mana, int32 StackCount);
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay ability")
		void OnBaseAttackDamageChange(float BaseAttackDamage, int32 StackCount);
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay ability")
		void OnSpeedMultiplierChange(float SpeedMultiplier, int32 StackCount);
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay ability")
		void OnFireBallCooldownChange(float FireBallCooldown, int32 StackCount);

	UFUNCTION(BlueprintCallable, Category = "Gameplay ability")
		void InitializeAbility(TSubclassOf<UGameplayAbility> AbilityToGet, int32 AbilityLevel);
	UFUNCTION(BlueprintCallable, Category = "Gameplay ability")
		void InitializeAbilityMulti(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAcquire, int32 AbilityLevel);

	UFUNCTION(BlueprintPure, Category = "Gameplay ability")
		void GetHealthValues(float& Health, float& MaxHealth);
	UFUNCTION(BlueprintPure, Category = "Gameplay ability")
		void GetManaValues(float& Mana, float& MaxMana);
	UFUNCTION(BlueprintPure, Category = "Gameplay ability")
		void GetBaseAttackDamageValues(float& BaseAttackDamage);
	UFUNCTION(BlueprintPure, Category = "Gameplay ability")
		void GetSpeedMultiplierValues(float& SpeedMultiplier);
	UFUNCTION(BlueprintPure, Category = "Gameplay ability")
		void GetFireBallCooldownValues(float& FireBallCooldown);
	UFUNCTION(BlueprintCallable, Category = "Gameplay ability")
		void RemoveAbilityWithTags(FGameplayTagContainer TagContainer);
	UFUNCTION(BlueprintCallable, Category = "Gameplay ability")
		void ChangeAbilityLevelWithTags(FGameplayTagContainer TagContainer, int32 NewLevel);
	UFUNCTION(BlueprintCallable, Category = "Gameplay ability")
		void CancelAbilityWithTags(FGameplayTagContainer WithTags, FGameplayTagContainer WithoutTags);
	UFUNCTION(BlueprintCallable, Category = "Gameplay ability")
		void AddLooseGameplayTag(FGameplayTag TagToAdd);
	UFUNCTION(BlueprintCallable, Category = "Gameplay ability")
		void RemoveLooseGameplayTags(FGameplayTag TagsToRemove);

protected:

	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);
			

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

