// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAS_testCharacter.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GAS_Component.h"
#include "GAS_AttributeSet.h"
#include "InputActionValue.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// AGAS_testCharacter

AGAS_testCharacter::AGAS_testCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)

	GAS_Component = CreateDefaultSubobject<UGAS_Component>("AbilitySystemComp");
	GAS_Component->SetIsReplicated(true);
	GAS_Component->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);
}

void AGAS_testCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	if (GAS_Component)
	{
		GAS_AttributeSet = GAS_Component->GetSet<UGAS_AttributeSet>();

		const_cast<UGAS_AttributeSet*>(GAS_AttributeSet)->HealthChangeDelegate.AddDynamic(this, &AGAS_testCharacter::OnHealthChangedNative);
		const_cast<UGAS_AttributeSet*>(GAS_AttributeSet)->ManaChangeDelegate.AddDynamic(this, &AGAS_testCharacter::OnManaChangedNative);
		const_cast<UGAS_AttributeSet*>(GAS_AttributeSet)->BaseAttackDamageChangeDelegate.AddDynamic(this, &AGAS_testCharacter::OnBaseAttackDamageChangedNative);
		const_cast<UGAS_AttributeSet*>(GAS_AttributeSet)->SpeedMultiplierChangeDelegate.AddDynamic(this, &AGAS_testCharacter::OnSpeedMultiplierChangedNative);
		const_cast<UGAS_AttributeSet*>(GAS_AttributeSet)->FireBallCooldownChangeDelegate.AddDynamic(this, &AGAS_testCharacter::OnFireBallCooldownChangedNative);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AGAS_testCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AGAS_testCharacter::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AGAS_testCharacter::Look);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AGAS_testCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	GAS_Component->InitAbilityActorInfo(this, this);
	InitializeAbilityMulti(InitialAbilities, 0);
}

void AGAS_testCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();
	GAS_Component->InitAbilityActorInfo(this, this);
}

void AGAS_testCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AGAS_testCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

UAbilitySystemComponent* AGAS_testCharacter::GetAbilitySystemComponent() const
{
	return GAS_Component;
}

void AGAS_testCharacter::OnHealthChangedNative(float Health, int32 StackCount)
{
	OnHealthChange(Health, StackCount);
}

void AGAS_testCharacter::OnManaChangedNative(float Mana, int32 StackCount)
{
	OnManaChange(Mana, StackCount);
}

void AGAS_testCharacter::OnBaseAttackDamageChangedNative(float BaseAttackDamage, int32 StackCount)
{
	OnBaseAttackDamageChange(BaseAttackDamage, StackCount);
}

void AGAS_testCharacter::OnSpeedMultiplierChangedNative(float SpeedMultiplier, int32 StackCount)
{
	OnSpeedMultiplierChange(SpeedMultiplier, StackCount);
}

void AGAS_testCharacter::OnFireBallCooldownChangedNative(float FireBallCooldown, int32 StackCount)
{
	OnFireBallCooldownChange(FireBallCooldown, StackCount);
}

void AGAS_testCharacter::InitializeAbility(TSubclassOf<UGameplayAbility> AbilityToGet, int32 AbilityLevel)
{
	if (HasAuthority() && GAS_Component)
	{
		GAS_Component->GiveAbility(FGameplayAbilitySpec(AbilityToGet, AbilityLevel, 0));
	}
}

void AGAS_testCharacter::InitializeAbilityMulti(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAcquire, int32 AbilityLevel)
{
	for (TSubclassOf<UGameplayAbility> AbilitItem : AbilitiesToAcquire)
	{
		InitializeAbility(AbilitItem, AbilityLevel);
	}
}

void AGAS_testCharacter::GetHealthValues(float& Health, float& MaxHealth)
{
	if (GAS_AttributeSet)
	{
		Health = GAS_AttributeSet->GetHealth();
		MaxHealth = GAS_AttributeSet->GetMaxHealth();
	}
}

void AGAS_testCharacter::GetManaValues(float& Mana, float& MaxMana)
{
	if (GAS_AttributeSet)
	{
		Mana = GAS_AttributeSet->GetMana();
		MaxMana = GAS_AttributeSet->GetMaxMana();
	}
}

void AGAS_testCharacter::GetBaseAttackDamageValues(float& BaseAttackDamage)
{
	if (GAS_AttributeSet)
	{
		BaseAttackDamage = GAS_AttributeSet->GetBaseAttackDamage();
	}
}

void AGAS_testCharacter::GetSpeedMultiplierValues(float& SpeedMultiplier)
{
	if (GAS_AttributeSet)
	{
		SpeedMultiplier = GAS_AttributeSet->GetSpeedMultiplier();
	}
}

void AGAS_testCharacter::GetFireBallCooldownValues(float& FireBallCooldown)
{
	if (GAS_AttributeSet)
	{
		FireBallCooldown = GAS_AttributeSet->GetFireBallCooldown();
	}
}

void AGAS_testCharacter::RemoveAbilityWithTags(FGameplayTagContainer TagContainer)
{
	TArray<FGameplayAbilitySpec*> MatchingAbilities;
	GAS_Component->GetActivatableGameplayAbilitySpecsByAllMatchingTags(TagContainer, MatchingAbilities, true);
	for (FGameplayAbilitySpec* Spec : MatchingAbilities)
	{
		GAS_Component->ClearAbility(Spec->Handle);
	}
}

void AGAS_testCharacter::ChangeAbilityLevelWithTags(FGameplayTagContainer TagContainer, int32 NewLevel)
{
	TArray<FGameplayAbilitySpec*> MatchingAbility;
	GAS_Component->GetActivatableGameplayAbilitySpecsByAllMatchingTags(TagContainer, MatchingAbility, true);
	for (FGameplayAbilitySpec* Spec : MatchingAbility)
	{
		Spec->Level = NewLevel;
	}
}

void AGAS_testCharacter::CancelAbilityWithTags(FGameplayTagContainer WithTags, FGameplayTagContainer WithoutTags)
{
	GAS_Component->CancelAbilities(&WithTags, &WithoutTags, nullptr);
}

void AGAS_testCharacter::AddLooseGameplayTag(FGameplayTag TagToAdd)
{
	GetAbilitySystemComponent()->AddLooseGameplayTag(TagToAdd);
	GetAbilitySystemComponent()->SetTagMapCount(TagToAdd, 1);
}

void AGAS_testCharacter::RemoveLooseGameplayTags(FGameplayTag TagsToRemove)
{
	GetAbilitySystemComponent()->RemoveLooseGameplayTag(TagsToRemove);
}